# Geocaching #

Geocaching is the GPS-based treasure hunt created in 2000. Your aim is to find containers hidden out in the wild by using GPS location services to get to their locations.

Some caches require you find their coordinates by solving a puzzle first. The purpose of this section is to provide a suite of functions to help in that effort, as well as other Geocaching-related code.

### Location functions ###

Locations can be provided in many different formats. This script provides functions to convert between types.

#### Parsing messy coordinates
```r
parseCoordinates(c("N55 55 33.3\"", "W003° 14' 9.54"))
        n         e 
55.925917 -3.235983
```

#### What 3 Words location service
```r
w3w(c("latest.fancied.escapes"))
[1] 55.68654 12.58752
```

### Decoding ###

Puzzles can sometimes include enciphered text. Here are the functions for decrypting it... or for creating enciphered text!

```r
rot("Hello world!")
[1] "Uryyb jbeyq!"
```

