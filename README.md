# Alun's portfolio #

Examples of my R work. Is there something you like, or would like to see?

### Google Distance API ###

I'm moving to Oxford. But where should I live? One way of looking at it is to see the public transport travel times to where my partner will be working. Here's a quick visualisation using data pulled from the [Google Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/start)

![](www/OxfordTravelTimes_colour_contour.png)